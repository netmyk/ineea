(function ($) {
    "use strict";
    $(window).on('load', function () {
            if ($().isotope && ($('._gallery').length > 0)) {
                var $container = $('._gallery'), // object that will keep track of options
                    isotopeOptions = {}, // defaults, used if not explicitly set in hash
                    defaultOptions = {
                        filter: '*', itemSelector: '._gallery-item', // set columnWidth to a percentage of container width
                        masonry: {
                        }
                    };

                $container.imagesLoaded().progress(function (instance, image) {
                    if (!image.isLoaded) {
                        return;
                    }
                    var p = $(image.img).closest('.hidden');
                    p.removeClass('hidden');
                    $container.addClass('is-loaded')
                    // set up Isotope
                    $container.each(function () {
                        $(this).isotope(defaultOptions);
                    });

                    $container.isotope('layout');
                });

                $('._gallery-filters li').on('click', function () {
                    var $this = $(this);
                    $('._gallery-filters .active').removeClass('active');
                    $this.addClass('active');

                    var selector = $this.attr('data-filter');
                    $container.isotope({
                        filter: selector, animationOptions: {
                            duration: 750, easing: 'linear', queue: false
                        }
                    });
                    return false;
                });
            }
    });
}(jQuery));