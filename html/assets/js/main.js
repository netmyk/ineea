
var $devicewidth = (window.innerWidth > 0) ? window.innerWidth : screen.width,
    $deviceheight = (window.innerHeight > 0) ? window.innerHeight : screen.height,
    $bodyEl = jQuery("body"),
    $htmlEl = jQuery("html"),
    $slider = jQuery('.ct-js-slick');

function validatedata($attr, $defaultValue) {
    "use strict";
    if ($attr !== undefined) {
        return $attr
    }
    return $defaultValue;
}
function parseBoolean(str, $defaultValue) {
    "use strict";
    if (str == 'true') {
        return true;
    } else if (str == "false") {
        return false;
    }
    return $defaultValue;
}


(function ($) {
    "use strict";

    if(document.getElementById('ct-js-wrapper')){
        var snapper = new Snap({
            element: document.getElementById('ct-js-wrapper')
        });
        snapper.settings({
            disable: "left",
            easing: 'ease',
            addBodyClasses: true
        });

    }


    var $stickyMenu = $('.sticky-menu'),
        $stickyMenuHeight = $stickyMenu.innerHeight() / 2,
        $stickyMenuOffset = $('.sticky-menu').offset().top;

    $(document).on('ready', function () {

        var tRotator = function (tInner) {
            if ($('.textRotator').length > 0){
                var $textRotator = $('.textRotator');
                $textRotator.each(function(){
                    var $textRotator__Inner = $(this).find('.textRotator__inner'),
                        $dataTime = $(this).attr('data-time');
                    // Table with text rotator elements
                    tInner = [
                        'Construction',
                        'Architecture',
                        'Geodesy'
                    ];
                    var counter = 0;

                    setInterval(function () {
                        $textRotator__Inner.fadeOut(function () {
                            $(this).html(tInner[counter = (counter + 1)%tInner.length]).fadeIn();
                        })
                    }, $dataTime);
                })
            }

        };
        tRotator();


        $('[data-toggle="tooltip"]').tooltip();

        // Browser detected

        if (jQuery.browser.mozilla){
            $('html').addClass('browser-mozilla')
        }
        if (jQuery.browser.webkit){
            $('html').addClass('webkit-detected')
        }
        if (jQuery.browser.msie){
            $('html').addClass('ie-detected')
        }
        if (jQuery.browser.safari){
            $('html').addClass('safari-detected')
        }


        // Snap Navigation in Mobile // -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        if ($devicewidth > 768 && document.getElementById('ct-js-wrapper')) {
            snapper.disable();
        }

        $(".navbar-toggle").on('click', function () {
            if($('body').hasClass('snapjs-right')){
                snapper.close();
            } else{
                snapper.open('right');
            }
        });


        $('._menuMobile ._menuMobile-navbar .dropdown > a').click(function() {
            return false; // iOS SUCKS
        })
        $('._menuMobile ._menuMobile-navbar .dropdown > a').click(function(){
            var $this = $(this);
            if($this.parent().hasClass('open')){
                $(this).parent().removeClass('open');
            } else{
                $('._menuMobile ._menuMobile-navbar .dropdown.open').toggleClass('open');
                $(this).parent().addClass('open');
            }
        })
        // Ignore Span when slider is using

        $slider.attr('data-snap-ignore', 'true');


        /* Height */

        if ($('[data-height]').length > 0) {
            $('[data-height]').each(function() {
                var $height, $this;
                $this = $(this);
                $height = $this.attr('data-height');
                if ($height.indexOf('%') > -1) {
                    $this.css('min-height', $deviceheight * parseInt($height, 10) / 100);
                } else {
                    $this.css('min-height', parseInt($height, 10) + 'px');
                }
            });
        }

        /* Background Color */

        if ($('[data-background]').length > 0) {
            $('[data-background]').each(function() {
                var $background, $backgroundmobile, $this;
                $this = $(this);
                $background = $(this).attr('data-background');
                $backgroundmobile = $(this).attr('data-background-mobile');
                if ($this.attr('data-background').substr(0, 1) === '#') {
                    $this.css('background-color', $background);
                } else if ($this.attr('data-background-mobile') && device.mobile()) {
                    $this.css('background-image', 'url(' + $backgroundmobile + ')');
                } else {
                    $this.css('background-image', 'url(' + $background + ')');
                }
            });
        }

        $('#video-player-param').lightGallery({
            youtubePlayerParams: {
                modestbranding: 0,
                showinfo: 1,
                rel: 1,
                controls: 1,
                videoMaxWidth: '1200px'
            }
        });

        /* Skrollr Parallax */
        $('[data-parallax]').each(function() {
            var $this, attr;
            $this = $(this);
            attr = $this.attr('data-parallax');
            $this.attr('data-top-bottom', 'background-position: 50% -' + attr + 'px');
            $this.attr('data-bottom-top', 'background-position: 50% ' + attr + 'px');
            $this.attr('data-center', 'background-position: 50% 0px');
        });

        /* Background position */

        if ($('[data-bg-position]').length > 0) {
            $('[data-bg-position]').each(function() {
                var $bgPosition, $this;
                $this = $(this);
                $bgPosition = $(this).attr('data-bg-position');
                if ($this.attr('data-bg-position') == '') {
                    $this.css('background-position', '50% 50%');
                }
                else if ($this.attr('data-bg-position')) {
                    $this.css('background-position', $bgPosition);
                }
            });
        }

        /* Color's */
        $("._js-color").each(function(){
            $(this).css("color", '#' + $(this).attr("data-color"))
        });

        /* Background-size */
        if ($('[data-bg-size]').length > 0) {
            $('[data-bg-size]').each(function() {
                var $bgSize, $this;
                $this = $(this);
                $bgSize = $(this).attr('data-bg-size');
                if ($this.attr('data-bg-size') == '') {
                    $this.css('background-size', 'cover');
                }
                else if ($this.attr('data-bg-position')) {
                    $this.css('background-size', $bgSize);
                }
            });
        }

        var $bg_position = $(this).attr("data-bg-position");
        $(this).css('background-position', $bg_position);


        if ($('.tabs-wrapper').length > 0){

            var $tabsNavigation = $('.tabs-wrapper').find('.tabs-navigation');

            if ($tabsNavigation) {

                $tabsNavigation.each(function(){

                    var $this = $(this),
                        $li = $this.find('li');

                    $li.each(function(){

                        var $this = $(this),
                            $tabContent = $($this.find('a').attr('href'));

                        if ($this.hasClass('is-active')) {
                            $tabContent.show();
                        } else {
                            $tabContent.hide();
                        }

                    });


                    $li.children('a').click(function(e) {

                        e.preventDefault();
                        var $this = $(this);
                        $li.removeClass('is-active');

                        $li.each(function() {
                            var $this = $(this);


                            $($this.find('a').attr('href')).hide();
                            $($this.find('a').attr('href')).removeClass('activate');
                        });

                        $this.parent().addClass('is-active');
                        var $effectIn = $('.tabs-wrapper').attr('data-effect-in');

                        if ($effectIn.length > 0){
                            $($this.attr('href')).addClass($effectIn + ' ' +  'animated activate').css({
                                display: 'block'
                            })
                        }
                    });

                });

            }

        }

        /* Navbar - making elements active */

        var url = window.location,
            pathname = window.location.pathname;
        $('.navbar').find('a').filter(function() {
            return this.href === url.href;
        }).closest('.navbar-nav > li').addClass('active');


        if (pathname === '/') {
            $('.navbar').find('a[href="index.html"]').closest('li').addClass('active');
        }


        $('#backToTop').on('click', function() {
            $("html, body").animate({ scrollTop: 0 }, 1000 , 'easeInOutExpo');
            return false;
        });

        /* Navbar mobile */
        if ($('.navbar-mobile').length > 0){
            var $navMobile = $('.navbar-mobile'),
                $navHamburger = $navMobile.find('.navbar-mobile-hamburger');

            $navHamburger.on('click', function(){
                var $this = $(this);
                $bodyEl.toggleClass('nav-mobile-active');
                if ($bodyEl.hasClass('nav-mobile-active')){
                    $('.nav-mobile').animate({
                        left: '0px'
                    }, 'slow')
                } else {
                    $('.nav-mobile').animate({
                        left: '-100%'
                    }, 'slow')
                }
            });

            $('.dropdown-mobile').children('a').each(function(){
                var $this = $(this);
                $this.on('click', function(e){
                    e.preventDefault();
                    $this.next().slideToggle('slow');
                    $this.toggleClass('is-active');
                })
            })

        }

        if (device.mobile() || device.ipad() || device.androidTablet() || device.isIE8) {
            $('.js-video').remove();
            $('body').removeClass('cssAnimate');
            $('.animated').css({
                opacity: '1'
            })
        }

        $('.cssAnimate .animated').appear(function () {
            var $this = $(this);

            $this.each(function () {
                if ($this.data('time') != undefined) {
                    setTimeout(function () {
                        $this.addClass('activate').addClass($this.data('fx'));
                    }, $this.data('time'));
                } else {
                    $this.addClass('activate').addClass($this.data('fx'));
                }
            });
        }, {accX: 50, accY: -200});

        $("img").each(function(){
    var $this = $(this);
    if ($this.attr("alt") === "" || this.hasAttribute("alt") === false){
        var $src = $this.attr('src'),
            $srcSplit = $src.split('/'),
            $srcFile = $srcSplit[$srcSplit.length-1],
            $data = $srcFile.split('.')[0],
            $dataFinal = $data.charAt(0).toUpperCase();
        $this.attr('alt', $dataFinal + $data.substring(1));
    }
});

        $('.ct-btn--scrollUp').on('click', function(e){
            e.preventDefault();
            $('html, body').animate({
                scrollTop : 0,
                specialEasing : 'easeInOutBounce'
            }, 1000);
        })

        // Contact form animation inputs

        var $contactForm = $('._contactForm');

        if ($contactForm.length > 0) {
            var $t = $(this);
            $t.find('.form-group').each(function () {
                $t.find('.form-control').on('focus keypress', function () {
                    var $t = $(this);
                    $t.addClass('active');
                    if ($t.val() === '') {
                        $t.removeClass('not-empty');
                    }
                    else {
                        $t.addClass('not-empty');
                    }
                });
                $t.find('.form-control').on('focusout', function () {
                    var $t = $(this);
                    $t.removeClass('active');
                    if ($t.val() === '') {
                        $t.removeClass('not-empty');
                    }
                })
            });
        }

            var iframe = $('#player1')[0];
            var player = $f(iframe);
            var status = $('.status');

            // When the player is ready, add listeners for pause, finish, and playProgress
            player.addEvent('ready', function() {
                status.text('ready');

                player.addEvent('pause', onPause);
                player.addEvent('finish', onFinish);
                player.addEvent('playProgress', onPlayProgress);
            });
            $('button._btn-play').bind('click', function() {
                player.api($(this).text().toLowerCase());
                $('.ct-mediaSection[data-type="video"]').addClass('moving');
                $('.ct-mediaSection[data-type="video"]').removeClass('stop-moving')
            });


        $('button._btn-pause').bind('click', function() {
            player.api($(this).text().toLowerCase());
            $('.ct-mediaSection[data-type="video"]').addClass('stop-moving');
            $('.ct-mediaSection[data-type="video"]').removeClass('moving')
        });


            function onPause(id) {
                status.text('paused');
            }

            function onFinish(id) {
                status.text('finished');
            }

            function onPlayProgress(data, id) {
                status.text(data.seconds + 's played');
            }

        if ($stickyMenu.hasClass('is-fixed')){
            $bodyEl.css('padding-top', $stickyMenuOffset + 'px')
        }

        // Gallery item expande

        var jsonUrl         = 'portfolio-elements.json',
            pathToProjects  = '../jsonProjects/',
            root            = $('body');
        $.ajax({
            dataType : 'json',
            type : 'GET',
            url : jsonUrl,
            success : function ( data) {
                var key;
                $('._gallery-item--expand').find('span.expanded').each(function () {
                    var $this = $(this);
                    $this.on('click', function() {
                        $('body').css('overflow', 'hidden');
                        var $attrValue = $this.attr('data-project-name');
                        for (key in data){
                            if ($attrValue === data[key].templateName){
                                $('.projectWrapper').addClass(data[key].templateName + ' ' + 'project-is-loaded');
                                $('.projectWrapper').fadeIn('fast');
                                $('.projectWrapper').append("<div class='inner'></div>");
                                $('.projectWrapper').find('.inner').append("<div class='_tableCell'></div>");
                                $('.projectWrapper').find('._tableCell').load(data[key].templateUrl);
                            }
                        }
                        return false
                    })
                })
            },
            error : function() {
                $('.error').text('Error')
            }
        });

        var projectWrapperMaker = function() {
            root.prepend("<div class='projectWrapper'></div>");
            $('.projectWrapper').append("<span class='close-btn'>x</span>");
            $('.close-btn').on('click', function() {
                $('body').css('overflow', 'auto');
                $('.projectWrapper').removeClass().addClass('projectWrapper');
                $('.projectWrapper').fadeOut('slow');
                $('.projectWrapper').find('.inner').remove();
            });
        };


        projectWrapperMaker(); // Project Wrapper init


}); // Document ready





    $(window).on('load', function(){
        if ($('#page-loader').length > 0){
            $('#page-loader').fadeOut().addClass('page-loader-hidden');
        }
        $('#preloader').fadeOut('slow');

        /* Skrollr */
        var skroll;
        if (!device.mobile() && !device.tablet() && !$htmlEl.hasClass('ie8')) {
            skroll = skrollr.init({
                forceHeight: false
            });
        }

    }); // window.load



    $(window).on('scroll', function() {
        var $this = $(this);

        var $scroll = $this.scrollTop();

        if ($scroll >= $stickyMenuOffset){
            $('.sticky-menu').addClass('is-fixed');
        } else {
            $('.sticky-menu').removeClass('is-fixed');
        }

        if ($stickyMenu.hasClass('is-fixed')){
            $bodyEl.css('padding-top', $stickyMenuHeight + 'px');
        } else {
            $bodyEl.css('padding-top', 0);
        }


    });

    $(window).on('resize', function(){
        if ($(window).width() < 767) {
            snapper.enable();
        } else{
            snapper.disable();
        }
    })

})(jQuery);





















