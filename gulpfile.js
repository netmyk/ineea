var gulp = require('gulp'),
    sass = require('gulp-sass'),
    jade = require('gulp-jade'),
    zip  = require('gulp-zip'),
    plumber = require('gulp-plumber'),
    browserSync = require('browser-sync');

/* Static Server */

gulp.task('browser-sync', function() {
    return browserSync.init({
        server: {
            baseDir: 'HTML'
        },
        ghostMode: false
    });
});

/* Sass */

gulp.task('sass', function(){
    return gulp.src('dev/sass/style.scss')
        .pipe(sass()) // Converts Sass to CSS with gulp-sass
        .pipe(gulp.dest('html/assets/css'))
        .pipe(browserSync.stream())
});

/* Jade */

gulp.task('html', function() {
    gulp.src('dev/jade/*.jade').pipe(plumber()).pipe(jade({
        pretty: true
    })).pipe(gulp.dest('HTML'));
});

/* Portfolio jade */

gulp.task('html-portfolio', function() {
    gulp.src('dev/jade/portfolio-elements/*.jade').pipe(plumber()).pipe(jade({
        pretty: true
    })).pipe(gulp.dest('html/portfolio-items'))
});

/* Zipping */

gulp.task('zip', function() {
    return gulp.src('html/**').pipe(zip('html.zip')).pipe(gulp.dest('html'))
});



gulp.task('watch', function() {
    gulp.watch('dev/**/*.jade', ['html']);
    gulp.watch('dev/**/*.jade', ['html-portfolio']);
    gulp.watch('dev/**/*.scss', ['sass']);
    gulp.watch('dev/sass/style.scss', ['sass']);
    gulp.watch('HTML/*.html', browserSync.reload);
});

gulp.task('default', [
    'watch',
    'browser-sync'
]) ;